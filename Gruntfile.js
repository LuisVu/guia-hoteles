module.exports = function (grunt) {

    require('time-grunt')(grunt);
    require('jit-grunt')(grunt, {
        useminPrepare: 'grunt-usemin'
    })

    grunt.initConfig({
        sass: {
            dist: {
                files: [{
                    expand: true,
                    cwd: 'css',
                    src: ['*.scss'],
                    dest: 'css',
                    ext: '.css'
                }]
            }
        },

        watch: {
            files: ['css/*.scss'],
            tasks: ['css']
        },

        browserSync: {
            dev: {
                bsFiles: {
                    src: [
                        'css/*.css',
                        '*.html',
                        'js/*.js'
                    ]
                },
                options: {
                    watchTask: true,
                    server: './'
                }
            }
        },

        imagemin: {
            dynamic: {
                files: [{
                    expand: true,
                    cwd: './',
                    src: 'imagenes/*.{png,jpg,gif}',
                    dest: 'dist/'
                }]
            }
        },
        copy: {
            html: {
                files: [{
                    expand: true, dot: true, cwd: './',
                    src: ['*.html'], dest: 'dist'
                }]
            },
            fonts: {//fonts se agrega para incluir iconos cunado se carga de open-iconic
                files: [{
                    expand: true,
                    dot:true,
                    cwd: 'node_modules/open-iconic/font',//se indica la raiz que sea font
                    src: 'fonts/*.*',//ya en la raíz dentro de la carpeta font hay una carpeta fonts}, busca todo su contenido y lo envia a dist
                    dest: 'dist'
                }]
            }
        },

        clean: { build: { src: ['dist/'] } },

        cssmin: { dist: {} },

        uglify: { dist: {} },

        filerev: {
            options: {
                encoding: 'utf8',
                algorithm: 'md5',
                length: 20
            },
            files: {
                src: ['dist/css/*.css',
                    'dist/js/*.js']
            }
        },

        concat: {
            options: {
                separator: ';'
            },
            dist: {}
        },

        useminPrepare: {
            foo: {
                dest: 'dist',
                src: ['index.html', 'contactos.html', 'precios.html', 'terminos-condiciones.html', 'about.html']
            },
            options: {
                flow: {
                    steps: {
                        css: ['cssmin'],
                        js: ['uglify']
                    },
                    post: {
                        css: [{
                            name: 'cssmin',
                            createConfig: function (context, block) {
                                var generated = context.options.generated;
                                generated.options = { keepSpecialComments: 0, rebase: false }
                            }
                        }]
                    }
                }
            }
        },

        usemin: {
            html: ['dist/index.html', 'dist/contactos.html', 'dist/precios.html', 'dist/terminos-condiciones.html', 'dist/about.html'],
            options: {
                assetsDir: ['dist', 'dist/css', 'dist/js']
            }
        }
    });

    //se comentan las siguientes cargas de plugind porque ya no se requieren  puesto que jit-grunt se encarga de la carga de los plugins con el require
    // grunt.loadNpmTasks('grunt-contrib-watch');//carga los paquetes o plugins 
    //  grunt.loadNpmTasks('grunt-browser-sync');
    //  grunt.loadNpmTasks('grunt-contrib-imagemin');
    //  grunt.loadNpmTasks('grunt-contrib-sass');//carga los paquetes o plugins
    grunt.registerTask('css', ['sass']);//agrega la tarea
    grunt.registerTask('default', ['browserSync', 'watch']);//se crea la tarea default,es la que se ejecuta cuando se escribe grunt en el cmd
    grunt.registerTask('img:compress', ['imagemin']);
    //agregamos la tarea build
    grunt.registerTask('build', [
        'clean', //Borramos el contenido de dist  
        'copy', //Copiamos los archivos html a dist   
        'imagemin', //Optimizamos imagenes y las copiamos a dist ,comprimir imagenes
        'useminPrepare', //Preparamos la configuracion de usemin  
        'concat',
        'cssmin',//comprimir css
        'uglify',
        'filerev', //Agregamos cadena aleatoria  
        'usemin' //Reemplazamos las referencias por los archivos generados por filerev  
    ]);

};