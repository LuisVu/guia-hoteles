$(function () {
    $('[data-toggle="tooltip"]').tooltip(); //se activa el tooltip  muestra un mensaje en nube emergente
    $('[data-toggle="popover"]').popover();//se activan los popover
    $('.carousel').carousel({
        interval: 2000
    });

    $('#exampleModal').on('show.bs.modal', function (e) {
        $('button.btn-reserva').prop('disabled', true);
        $('button.btn-reserva').removeClass('btn-primary');
        $('button.btn-reserva').addClass('btn-outline-success');
        console.log("inició a mostrarse");
    });

    $('#exampleModal').on('shown.bs.modal', function (e) {
        console.log("terminó a mostrarse");
    });

    $('#exampleModal').on('hide.bs.modal', function (e) {
        $('button.btn-reserva').prop('disabled', false);
        $('button.btn-reserva').removeClass('btn-outline-success');
        $('button.btn-reserva').addClass('btn-primary');
        console.log("inició a ocultarse");
    });

    $('#exampleModal').on('hidden.bs.modal', function (e) {
        console.log("terminó de ocultarse");
    });

});